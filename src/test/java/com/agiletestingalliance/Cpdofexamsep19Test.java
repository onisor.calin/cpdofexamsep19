package com.agiletestingalliance;

import static org.junit.Assert.assertEquals;

import com.agiletestingalliance.Usefulness;
import com.agiletestingalliance.Duration;
import com.agiletestingalliance.AboutCPDOF;
import com.agiletestingalliance.MinMax;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

public class Cpdofexamsep19Test
{
    @Test
    public void runDuration() throws Exception {
        
          assertEquals("Result", "CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and can't dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams.", new Duration().dur());


	
    }
           @Test
    public void runAboutCPDOF() throws Exception {
      	
          		assertEquals("Result","CP-DOF certification program covers end to end DevOps Life Cycle practically. CP-DOF is the only globally recognized certification program which has the following key advantages: <br> 1. Completely hands on. <br> 2. 100% Lab/Tools Driven <br> 3. Covers all the tools in entire lifecycle <br> 4. You will not only learn but experience the entire DevOps lifecycle. <br> 5. Practical Assessment to help you solidify your learnings." ,new AboutCPDOF().desc());
      }
    
        @Test
    public void runUsefulness() throws Exception {
      		assertEquals("Result","DevOps is about transformation, about building quality in, improving productivity and about automation in Dev, Testing and Operations. <br> <br> CP-DOF is a one of its kind initiative to marry 2 distinct worlds of Agile and Operations together. <br> <br> <b>CP-DOF </b> helps you learn DevOps fundamentals along with Continuous Integration and Continuous Delivery and deep dive into DevOps concepts and mindset.", new Usefulness().desc());
      
      }
        @Test
    public void runMinMax() throws Exception {
    	    		assertEquals("Result",4,new MinMax().compare(3,4));
              assertEquals("Result",4,new MinMax().compare(4,3));    	
        
      		
      }
      
      
      
}